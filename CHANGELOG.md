# Changelog


## v0.1.3 (2022-12-19)

### Features

* Add port autodetection. [Stavros Korokithakis]


## v0.1.2 (2021-09-11)

### Fixes

* Make major X axis locators sparser. [Stavros Korokithakis]

* Change display to Ah. [Stavros Korokithakis]

* Use duration (in seconds) instead of timestamp. [Stavros Korokithakis]

* Change the legend location. [Stavros Korokithakis]


## v0.1.1 (2021-09-08)

### Features

* Add twin axes when plotting. [Stavros Korokithakis]


## v0.1.0 (2021-09-08)

### Features

* Add changelog. [Stavros Korokithakis]


